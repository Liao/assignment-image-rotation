#ifndef BMP_H
#define BMP_H  
#include <stdint.h>
#include <stdio.h> 
struct pixel{
	uint8_t b,g,r;
};
struct image{
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};
#endif  
