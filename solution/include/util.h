#ifndef UTIL_H
#define UTIL_H
#include "bmp.h"
#include <malloc.h>
void destory_image(struct image * image);
void destory_pixels(struct pixel* pixels); 
#endif
