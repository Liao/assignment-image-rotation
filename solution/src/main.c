#include "io.h"
#include "translator.h"
#include "util.h"

int main( int argc, char** argv ) {
	if(argc != 3){
		fprintf(stderr,"Should take 2 arguments-resouce and target\n");
		return 1;
	}
	char *in = argv[1];
	char *out = argv[2];

	FILE *resource;
	FILE *target;
	bool read_resource = open_file(&resource, in, "rb");
	bool read_target = open_file(&target, out, "wb");
	if(!read_resource||!read_target){
		fprintf(stderr,"Arguments are not available!\n");
		close_file(&resource);
		close_file(&target);
		return 1;
	}

	struct image copy;
	enum read_status read_result = from_bmp(resource,&copy);
	if(read_result){
		destory_pixels(copy.data);
		close_file(&resource);
		close_file(&target);
		if(read_result == READ_INVALID_SIGNATURE) fprintf(stderr,"Signature invalid!\n");
		if(read_result == READ_INVALID_BITS) fprintf(stderr,"Bits invalid!\n");
		if(read_result == READ_INVALID_HEADER) fprintf(stderr,"Header invalid!\n");
		if(read_result == READ_INVALID_PLANES) fprintf(stderr,"Planes invalid!\n");
		if(read_result == READ_ERROR) fprintf(stderr,"Something invalid!\n");
		fprintf(stderr, "Bmp file unavailable.Format wrong.\n");
		return 1;
	}

	struct image result =rotate(copy);
	enum write_status write_result = to_bmp(target,&result);
	if(write_result){
		destory_pixels(result.data);
		destory_pixels(copy.data);
		close_file(&resource);
		close_file(&target);
		if(write_result == WRITE_ERROR) fprintf(stderr,"Failed to write!\n");
		fprintf(stderr, "Fail to write\n");
		return 1;
	} 
	
	bool fail_close_resource = close_file(&resource);
	bool fail_close_target = close_file(&target);
	destory_pixels(result.data);
	destory_pixels(copy.data);	
	if(!fail_close_resource||!fail_close_target){
		fprintf(stderr, "Fail to close files\n"); 
		return 1;	
	}

	return 0;	
}
