#include "util.h"
#include <malloc.h>
void destory_pixels(struct pixel* pixels){
	free(pixels);
}
void destory_image(struct image * image){
	destory_pixels(image->data);
	free(image);
} 
