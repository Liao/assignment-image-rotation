#include "io.h"
#include "header.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#define bfTYPE 0x4D42
#define biSIZE 40
#define biBITCOUNT 24
#define biPLANES 1
static size_t get_padding(const struct image* img){
	 return 4 - (img->width)*3%4;
}
static void print_header(struct bmp_header header){
	printf("%s","\n");
	printf("Signature is %x.\n",header.bfType);
	printf("Size of file is %d bytes\n", header.bfileSize);
	printf("Reserved is %d\n",header.bfReserved);
	printf("Offset is %d\n", header.bOffBits);
	printf("Size of struct header is %d bytes\n", header.biSize);
	printf("Width of picture is %d\n", header.biWidth);
	printf("Heigth of picture is %d\n", header.biHeight);
	printf("Planes is %d\n",header.biPlanes);
	printf("Bit count is %d\n",header.biBitCount);
	printf("Compression is %d\n",header.biCompression);
	printf("Size of image is %d\n",header.biSizeImage);
	printf("Pixels per meter by x is %d\n",header.biXPelsPerMeter);
	printf("Pixels per meter by y is %d\n",header.biYPelsPerMeter);
	printf("Color used is %d\n",header.biClrUsed);
	printf("Importance of color is %d\n",header.biClrImportant);
	printf("%s","\n"); 	
}
static bool painting(struct bmp_header header, struct image const* img, FILE* out){
	if(!img||!out){
		return false;
	}	
	size_t padding = get_padding(img);	
	if(fwrite(&header,sizeof(struct bmp_header),1,out)<1){
		return false;
	};
	for(size_t i=0; i < img->height;i++){
		if(fwrite((char*)img->data+i*3*(img->width),3,img->width,out)<3)return false;
		if(fseek(out,padding,SEEK_CUR)!=0)return false;	
	}
	return true;
}	
static bool set_header(struct bmp_header* header, struct image const* img){
	size_t padding = get_padding(img);
	if(!header||!img){
		return false;
	}	
	header->bfType =  bfTYPE;	
	header->biSizeImage=(img->width*sizeof(struct pixel)+padding)*img->height;
	header->bOffBits = sizeof(struct bmp_header); 
	header->biSize = biSIZE;
	header->biWidth = img->width;
	header->biHeight = img->height;
	header->biPlanes = biPLANES;
	header->biBitCount = biBITCOUNT;
	header->bfileSize = header->biSizeImage+sizeof(struct bmp_header);
	header->bfReserved = 0;
	header->biCompression=0;
	header->biXPelsPerMeter=0;
	header->biYPelsPerMeter=0; 
	header->biClrUsed = 0;
	header->biClrImportant = 0;
	return true;
} 	
bool open_file(FILE **file, const char *name, const char *mode){
	if(!file||!name||!mode){
		return true;
	}
	*file = fopen(name,mode);
	return *file != NULL;
}
bool close_file(FILE **file){
    if(*file == NULL){
        return true;
    }
    if(fclose(*file)==EOF){
        return false;
    }else {
		return true;
	}
}
enum read_status from_bmp(FILE* in, struct image* img){
	if(!in||!img){
		return READ_ERROR;
	}// make sure that all not NULL
	struct bmp_header header={0};
	if(fread(&header,sizeof(struct bmp_header), 1,in)<1){
		return READ_INVALID_HEADER;
	}	
	if(header.bfType!=bfTYPE) return READ_INVALID_SIGNATURE;
	if(header.biBitCount != biBITCOUNT) return READ_INVALID_BITS;
	if(header.biPlanes != biPLANES) return READ_INVALID_PLANES;
	img->height = header.biHeight;
	img->width=header.biWidth;
	img->data=(struct pixel*)malloc(sizeof(struct pixel)*(header.biHeight)*(header.biWidth));
	if(!img->data){
		fprintf(stderr,"Cannot allocate memory be transform!\n");
		return READ_ERROR;
	}
	const size_t padding = get_padding(img); 	
	for(size_t i = 0; i<img->height;i++){
		if(fread((char *)img->data + i * img->width * 3, 3, img->width, in)<3)return READ_INVALID_BITS;
		if(fseek(in, padding, SEEK_CUR)!=0){
			return READ_INVALID_BITS;
		}
	}
	print_header(header);
	return READ_OK;
}

enum write_status to_bmp(FILE *out,struct image const* img){
	if(!out||!img){
		return WRITE_ERROR;
	}
	struct bmp_header header={0};
	struct bmp_header * pointer_header = &header;	
	const bool is_set = set_header(pointer_header,img);
	if(!is_set){
		return WRITE_ERROR;
	}
	const bool is_paint = painting(header,img,out);
	if(!is_paint){
		return WRITE_ERROR;
	}
	print_header(header);
	return WRITE_OK;	
}

