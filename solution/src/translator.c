#include "translator.h" 
#include <malloc.h>

struct image rotate(struct image const source){
	const uint64_t width = source.width;
	const uint64_t height = source.height;
	struct pixel* data = source.data;
	struct image result; //tried to use poninter and return *result, but we can't free this pointer later;
	result.width=height;
	result.height=width;
	result.data =  malloc(sizeof(struct pixel)*width*height);
	if(!result.data){
		    fprintf(stderr, "Cannot allocate memory by transform!");
		    return (struct image) {0};
	}
	for(size_t i = 0; i< height;i++){
		for(size_t j=0;j<width;j++){
			*(result.data + j*height + (height - 1 - i) ) = *(data+i*width+j);
		}
	}
	return result;	
}
